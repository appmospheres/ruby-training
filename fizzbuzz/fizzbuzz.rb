def fizzbuzz(n)
  s = ""
  1.upto(n) do |i|
    if i % 15 == 0
      s += "fizzbuzz\n"
    elsif i % 5 == 0
      s += "buzz\n"
    elsif i % 3 == 0
      s += "fizz\n"
    else
      s += "#{i}\n"
    end
  end
  s
end

def as_fizzbuzz(n)
  if n % 15 == 0
    "fizzbuzz"
  elsif n % 5 == 0
    "buzz"
  elsif n % 3 == 0
    "fizz"
  else
    "#{n}"
  end
end

class FizzBuzz

  def initialize(to)
    @to = to
  end

  def to_a
    (1..@to).map do |i|
      if i % 15 == 0
        "fizzbuzz"
      elsif i % 5 == 0
        "buzz"
      elsif i % 3 == 0
        "fizz"
      else
        "#{i}"
      end
    end
  end

end

class Integer
  def as_fizzbuzz
    if self % 15 == 0
      "fizzbuzz"
    elsif self % 5 == 0
      "buzz"
    elsif self % 3 == 0
      "fizz"
    else
      "#{self}"
    end
  end
end
