gem "minitest"
require 'minitest/autorun'

require_relative './fizzbuzz.rb'

class FizzBuzzTest < Minitest::Test

  def test_fizzbuzz
    assert_equal "1\n2\nfizz\n4\nbuzz\nfizz\n7\n8\nfizz\nbuzz\n", fizzbuzz(10)
  end

  def test_as_fizzbuzz
    assert_equal "1", as_fizzbuzz(1)
    assert_equal "fizz", as_fizzbuzz(3)
    assert_equal "buzz", as_fizzbuzz(5)
    assert_equal "fizzbuzz", as_fizzbuzz(15)
  end

end


describe FizzBuzz do

  it "should return an fizzbuzz array" do
    FizzBuzz.new(10).to_a.must_equal ["1", "2", "fizz", "4", "buzz", "fizz", "7", "8", "fizz", "buzz"]
  end

end

describe Integer do

  it "should map a number to its fizzbuzz equivalent" do
    1.as_fizzbuzz.must_equal "1"
    3.as_fizzbuzz.must_equal "fizz"
    5.as_fizzbuzz.must_equal "buzz"
    15.as_fizzbuzz.must_equal "fizzbuzz"
  end

end
