# Environment
  1. Go to [http://railsinstaller.org](http://railsinstaller.org) and install the 3.0.0 alpha. Make sure you don't skip the SSH key generation at the end.
     * download sqlite3 from http://sqlite.org (both source - put it in c:\sqlite and binaries - put them in c:\windows\system32)
     * upgrade to latest rubygems (gem update --system), uninstall sqlite3 gem (gem uninstall sqlite3) and reinstall with:
         gem install sqlite3 --platform=ruby -- --with-sqlite3-include=c:\sqlite --with-sqlite3-lib=c:\windows\system32
  2. Go to [http://bitbucket.org](http://bitbucket.org)
     - create a bitbucket account; add the SSH public key; create a new repository for the ruby koans homework
  3. Ruby Koans
     Download the zip with the exercises from [http://rubykoans.com](http://rubykoans.com) and unarchive it.
     - cd koans; git init; git add .; git commit -m "initial commit"
     - git remote add .... (URL for the koans bitbucket project)
     - git push -u origin --all
     - After any changes, run a 'git status' to preview the changes, then 'git add .' to add them all to the next commit,
      commit them with 'git commit' and push the changes to the remote repository with 'git push origin master'

# Ruby Basics

* the repl/irb
* everything is an object
* constants, variables, naming conventions
* basic syntax, control structures
* numbers, strings, string interpolation, symbols
* arrays, hashes
* classes/modules/mixins

         - object/attributes
         - instance_variables: attr_accessor, attr_reader, attr_writer
         - globals ($ vars), class variables (@@)
         - object methods vs class methods

* iterators & blocks (single-line vs. multiline)
* Enumerable module: each, first/last, select, map
* basic I/O (file open, readlines, each_line iterator)
* exceptions - begin/rescue; raise
* ruby standard library; require vs. load
* bignum, csv, json, yaml

### Advanced

* minitest basics
* object equality, Comparable module
* self methods on objects and modules
* lambdas & procs
* regular expressions
* basic metaprogramming: method_missing
* rubygems & standard gems for web apps: sinatra, rails, heroku
* rake, bundler

# Homework
Expenses - parse the expenses.csv file and print some meaningful statistics

* total spent per user
* total spent per user/per category
* total spent per year/month

# External resources
### Sites

* [http://ruby-doc.org](http://ruby-doc.org) - main site for Ruby core and standard library documentation
* [http://tryruby.org](http://tryruby.org) - online ruby repl, basic introduction to core concepts
* [http://sourcetreeapp.com](http://sourcetreeapp.com) - good GUI for GIT, integrates well with BitBucket
* [http://sublimetext.com](http://sublimetext.com) - a very good multiplatform editor
* [http://git-scm.com/book](http://git-scm.com/book) - a reference book for GIT

### Books

* [http://pragprog.com/book/ruby/programming-ruby](http://pragprog.com/book/ruby/programming-ruby) - the reference manual

### Screencasts

* [http://railstutorial.org](http://railstutorial.org)
* [http://www.railscasts.com](http://www.railscasts.com)
* [http://www.rubytapas.com](http://www.rubytapas.com)
